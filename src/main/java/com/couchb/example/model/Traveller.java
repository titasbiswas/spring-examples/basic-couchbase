package com.couchb.example.model;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
@EqualsAndHashCode
public class Traveller {
	private String id, firstName, lastName, middleName, birthDate, gender, loyaltyCode, passengerID, stateRoom, age;

}
