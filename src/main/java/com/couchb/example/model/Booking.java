package com.couchb.example.model;

import java.util.ArrayList;
import java.util.List;

import org.springframework.data.annotation.Id;
import org.springframework.data.couchbase.core.mapping.Document;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;


@Document
@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
@EqualsAndHashCode
public class Booking {
	
	//@Id
	//private String id;
	@Id
	private String bookingCode;
	
	private String bookingPlacedBy;
	private String bookingOwner;
	private String bookingDate;
	private String bookingCurrency;
	private String sailingCurrency;
	private String sailingId;
	private String bookingStatus;
	private String subTotal;
	private String totalPrice;
	private String totalTax;
	private String totalDiscount;
	private Reservation reservation;
	@Builder.Default
	private List<Traveller> travellers = new ArrayList<>();
	@Builder.Default
	private List<BookingEntry> bookingEntries = new ArrayList<>();
	

}
