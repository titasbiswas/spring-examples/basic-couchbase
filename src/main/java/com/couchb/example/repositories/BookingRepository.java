package com.couchb.example.repositories;

import org.springframework.data.couchbase.core.query.N1qlPrimaryIndexed;
import org.springframework.data.couchbase.core.query.ViewIndexed;
import org.springframework.data.couchbase.repository.CouchbasePagingAndSortingRepository;

import com.couchb.example.model.Booking;

@N1qlPrimaryIndexed
@ViewIndexed(designDoc = "booking")
public interface BookingRepository extends CouchbasePagingAndSortingRepository<Booking, String>{

}
