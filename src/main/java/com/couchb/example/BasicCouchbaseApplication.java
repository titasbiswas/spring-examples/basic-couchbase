package com.couchb.example;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class BasicCouchbaseApplication {

	public static void main(String[] args) {
		SpringApplication.run(BasicCouchbaseApplication.class, args);
	}
}
